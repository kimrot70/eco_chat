import 'package:eco_chat/chat_screen.dart';
import 'package:eco_chat/login_screen.dart';
import 'package:eco_chat/registration_screen.dart';
import 'package:eco_chat/welcome_screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(EcoChat());

class EcoChat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: WelcomeScreen.id,
      routes: {
        WelcomeScreen.id: (context) => WelcomeScreen(),
        LoginScreen.id: (context) => LoginScreen(),
        RegistrationScreen.id: (context) => RegistrationScreen(),
        ChatScreen.id: (context) => ChatScreen(),
      },
    );
  }
}
